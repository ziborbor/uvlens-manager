﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kindy2.Models
{
    public class KindyHome
    {
        public int KindyID { get; set; }
        public string KindyN { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Status { get; set; }

    }
}