﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kindy2.Models;

namespace Kindy2.Controllers
{
    public class UVDatasController : Controller
    {
        private SensorEntities db = new SensorEntities();

        // GET: UVDatas
        public ActionResult Index()
        {
            return View(db.UVDatas.ToList());
        }

        // GET: UVDatas/Details/5
        public ActionResult Details(string id)
        {
           if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //only the ones that match with the sensorID will be shown

            int temp = Convert.ToInt32(id);
            var query = from a in db.UVDatas
                        where a.SensorID == temp
                        orderby a.Time
                        select a;

            return View(query.ToList());
        
        }

        // GET: UVDatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UVDatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Time,SensorID,UVIndex,SensorMV,Version,CreatedAt,UpdatedAt,Deleted")] UVData uVData)
        {
            if (ModelState.IsValid)
            {
                db.UVDatas.Add(uVData);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(uVData);
        }

        // GET: UVDatas/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UVData uVData = db.UVDatas.Find(id);
            if (uVData == null)
            {
                return HttpNotFound();
            }
            return View(uVData);
        }

        // POST: UVDatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Time,SensorID,UVIndex,SensorMV,Version,CreatedAt,UpdatedAt,Deleted")] UVData uVData)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uVData).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "UVDatas", new { id = uVData.SensorID });
            }
            return View(uVData);
        }

        // GET: UVDatas/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UVData uVData = db.UVDatas.Find(id);
            if (uVData == null)
            {
                return HttpNotFound();
            }
            return View(uVData);
        }

        // POST: UVDatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            UVData uVData = db.UVDatas.Find(id);
            db.UVDatas.Remove(uVData);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
