﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kindy2.Models;

namespace Kindy2.Controllers
{
    public class HomeController : Controller
    {
        private SP64Entities db = new SP64Entities();

        //Pass the query into the view
        
        public ActionResult Index()
        {
            IList<KindyHome> KiList = new List<KindyHome>();

            // Database Query (left join table for Kindergartens and ServiceHistories)
            var Qks = (from ki in db.Kindergartens
                       join sh in db.ServiceHistories on ki.KindyID equals sh.KID
                       into t
                       from rt in t.DefaultIfEmpty()
                       orderby ki.KindyID
                       select new
                       {
                           ki.KindyID,
                           ki.KindyN,
                           ki.Lat,
                           ki.Lng,
                           Status = ((rt.Status == null) ? 0 : rt.Status) //if Status is null, will convert to 0
                       });
            var Klishers = Qks.ToList();
            foreach (var Kdata in Klishers)
            {
                KiList.Add(new KindyHome()
                {
                    KindyID = Kdata.KindyID,
                    KindyN = Kdata.KindyN,
                    Lat = Kdata.Lat,
                    Lng = Kdata.Lng,
                    Status = Kdata.Status
                });
            }

            return View(KiList);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Settings()
        {
            ViewBag.Message = "Setting page.";

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        
        public ActionResult KindyList()
        {
            // partial view structure
            using (Models.SP64Entities dc = new Models.SP64Entities())
            {
                var v = dc.Kindergartens.OrderBy(a => a.KindyN).ToList();
                return PartialView("KindyList", v);
            }
            
        }


    }
}