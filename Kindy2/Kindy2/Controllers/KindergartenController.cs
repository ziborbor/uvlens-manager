﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kindy2.Models;

namespace Kindy2.Controllers
{
    public class KindergartenController : Controller
    {
        private SP64Entities db = new SP64Entities();

        // GET: Kindergarten
        public ActionResult Index()
        {
            var kindergartens = db.Kindergartens.Include(k => k.UVSensor);
            return View(kindergartens.ToList());
        }

        // GET: Kindergarten/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kindergarten kindergarten = db.Kindergartens.Find(id);
            if (kindergarten == null)
            {
                return HttpNotFound();
            }
            return View(kindergarten);
        }

        // GET: Kindergarten/Create
        public ActionResult Create()
        {
            ViewBag.SensorId = new SelectList(db.UVSensors, "UVId", "UVId");
            return View();
        }

        // POST: Kindergarten/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KindyID,KindyN,ContactN,Phone,Email,Address,Suburb,City,PostC,Region,Country,Lat,Lng,SensorId")] Kindergarten kindergarten)
        {
            if (ModelState.IsValid)
            {
                db.Kindergartens.Add(kindergarten);
                db.SaveChanges();
                return RedirectToAction("Details", "Kindergarten", new { id = kindergarten.KindyID });
            }

            ViewBag.SensorId = new SelectList(db.UVSensors, "UVId", "UVId", kindergarten.SensorId);
            return View(kindergarten);
        }

        // GET: Kindergarten/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kindergarten kindergarten = db.Kindergartens.Find(id);
            if (kindergarten == null)
            {
                return HttpNotFound();
            }
            ViewBag.SensorId = new SelectList(db.UVSensors, "UVId", "UVId", new { kindergarten.KindyID });
            return View(kindergarten);
        }

        // POST: Kindergarten/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KindyID,KindyN,ContactN,Phone,Email,Address,Suburb,City,PostC,Region,Country,Lat,Lng,SensorId")] Kindergarten kindergarten)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kindergarten).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "Kindergarten", new { id = kindergarten.KindyID });
            }
            ViewBag.SensorId = new SelectList(db.UVSensors, "UVId", "UVId", kindergarten.SensorId);
            return View(kindergarten);
        }

        // GET: Kindergarten/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kindergarten kindergarten = db.Kindergartens.Find(id);
            if (kindergarten == null)
            {
                return HttpNotFound();
            }
            return View(kindergarten);
        }

        // POST: Kindergarten/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kindergarten kindergarten = db.Kindergartens.Find(id);
            db.Kindergartens.Remove(kindergarten);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
