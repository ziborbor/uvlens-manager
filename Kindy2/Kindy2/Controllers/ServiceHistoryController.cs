﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kindy2.Models;

namespace Kindy2.Controllers
{
    public class ServiceHistoryController : Controller
    {
        private SP64Entities db = new SP64Entities();

        // GET: ServiceHistory
        public ActionResult Index()
        {
            var serviceHistories = db.ServiceHistories.Include(s => s.Kindergarten).Include(s => s.StatusS);
            return View(serviceHistories.ToList());
        }

        // GET: ServiceHistory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceHistory serviceHistory = db.ServiceHistories.Find(id);
            if (serviceHistory == null)
            {
                return HttpNotFound();
            }
            return View(serviceHistory);
        }

        // GET: ServiceHistory/Create
        public ActionResult Create()
        {
            ViewBag.KID = new SelectList(db.Kindergartens, "KindyID", "KindyN");
            ViewBag.Status = new SelectList(db.StatusSes, "StatusId", "Level");
            return View();
        }

        // POST: ServiceHistory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SID,Date,Description,Status,KID")] ServiceHistory serviceHistory)
        {
            if (ModelState.IsValid)
            {
                db.ServiceHistories.Add(serviceHistory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KID = new SelectList(db.Kindergartens, "KindyID", "KindyN", serviceHistory.KID);
            ViewBag.Status = new SelectList(db.StatusSes, "StatusId", "Level", serviceHistory.Status);
            return View(serviceHistory);
        }

        // GET: ServiceHistory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceHistory serviceHistory = db.ServiceHistories.Find(id);
            if (serviceHistory == null)
            {
                return HttpNotFound();
            }
            ViewBag.KID = new SelectList(db.Kindergartens, "KindyID", "KindyN", serviceHistory.KID);
            ViewBag.Status = new SelectList(db.StatusSes, "StatusId", "Level", serviceHistory.Status);
            return View(serviceHistory);
        }

        // POST: ServiceHistory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SID,Date,Description,Status,KID")] ServiceHistory serviceHistory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceHistory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KID = new SelectList(db.Kindergartens, "KindyID", "KindyN", serviceHistory.KID);
            ViewBag.Status = new SelectList(db.StatusSes, "StatusId", "Level", serviceHistory.Status);
            return View(serviceHistory);
        }

        // GET: ServiceHistory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceHistory serviceHistory = db.ServiceHistories.Find(id);
            if (serviceHistory == null)
            {
                return HttpNotFound();
            }
            return View(serviceHistory);
        }

        // POST: ServiceHistory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceHistory serviceHistory = db.ServiceHistories.Find(id);
            db.ServiceHistories.Remove(serviceHistory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult StatusT(int? id)
        {
            using (Models.SP64Entities dadc = new Models.SP64Entities())
            {
                var v = dadc.ServiceHistories.Where(s => s.KID == id).Include(s => s.Kindergarten).Include(s => s.StatusS);
                return PartialView("StatusT", v.ToList());
            }

        }

    }
}
