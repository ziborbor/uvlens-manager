﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kindy2.Models;

namespace Kindy2.Controllers
{
    public class StatusSController : Controller
    {
        private SP64Entities db = new SP64Entities();

        // GET: StatusS
        public ActionResult Index()
        {
            return View(db.StatusSes.ToList());
        }

        // GET: StatusS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StatusS statusS = db.StatusSes.Find(id);
            if (statusS == null)
            {
                return HttpNotFound();
            }
            return View(statusS);
        }

        // GET: StatusS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StatusS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StatusId,Level")] StatusS statusS)
        {
            if (ModelState.IsValid)
            {
                db.StatusSes.Add(statusS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(statusS);
        }

        // GET: StatusS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StatusS statusS = db.StatusSes.Find(id);
            if (statusS == null)
            {
                return HttpNotFound();
            }
            return View(statusS);
        }

        // POST: StatusS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StatusId,Level")] StatusS statusS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(statusS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(statusS);
        }

        // GET: StatusS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StatusS statusS = db.StatusSes.Find(id);
            if (statusS == null)
            {
                return HttpNotFound();
            }
            return View(statusS);
        }

        // POST: StatusS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StatusS statusS = db.StatusSes.Find(id);
            db.StatusSes.Remove(statusS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
