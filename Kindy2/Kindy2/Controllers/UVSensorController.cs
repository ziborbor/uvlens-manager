﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kindy2.Models;

namespace Kindy2.Controllers
{
    public class UVSensorController : Controller
    {
        private SP64Entities db = new SP64Entities();

        // GET: UVSensor
        public ActionResult Index()
        {
            return View(db.UVSensors.ToList());
        }

        // GET: UVSensor/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            int temp = Convert.ToInt32(id);
            var query = from a in db.UVSensors
                        where a.SensorId == temp
                        orderby a.Time
                        select a;

            return View(query.ToList());
        }

        // GET: UVSensor/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UVSensor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UVId,SensorId,Time,UVIndex,Deleted,SensorMV,Column1,Column2")] UVSensor uVSensor)
        {
            if (ModelState.IsValid)
            {
                db.UVSensors.Add(uVSensor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uVSensor);
        }

        // GET: UVSensor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UVSensor uVSensor = db.UVSensors.Find(id);
            if (uVSensor == null)
            {
                return HttpNotFound();
            }
            return View(uVSensor);
        }

        // POST: UVSensor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UVId,SensorId,Time,UVIndex,Deleted,SensorMV,Column1,Column2")] UVSensor uVSensor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uVSensor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uVSensor);
        }

        // GET: UVSensor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UVSensor uVSensor = db.UVSensors.Find(id);
            if (uVSensor == null)
            {
                return HttpNotFound();
            }
            return View(uVSensor);
        }

        // POST: UVSensor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UVSensor uVSensor = db.UVSensors.Find(id);
            db.UVSensors.Remove(uVSensor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
