﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kindy2.Startup))]
namespace Kindy2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
