﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kindy2
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class JoinEntities : DbContext
    {
        public JoinEntities()
            : base("name=JoinEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Kindergarten> Kindergartens { get; set; }
        public virtual DbSet<ServiceHistory> ServiceHistories { get; set; }
        public virtual DbSet<StatusS> StatusSes { get; set; }
    }
}
